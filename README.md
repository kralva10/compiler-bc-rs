Usage:
```sh
./build.sh                        # builds the project
./fml.sh input.json > output.bc   # compiles the input json into FML bytecode
./fml.sh -d input.json            # compiles the input and produces debug printout of bytecode
```