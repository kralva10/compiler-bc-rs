#!/bin/bash
which cargo;
if test "$?" -ne 0
then
curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh
fi
source $HOME/.cargo/env
cargo build --release
