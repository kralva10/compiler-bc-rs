#!/bin/bash
if [ "$1" = "-d" ]; then
  ./target/release/compiler-bc-rs -d "$2"
else
  ./target/release/compiler-bc-rs "$1"
fi
