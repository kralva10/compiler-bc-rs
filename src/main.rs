pub mod ast;
pub mod compiler;

use crate::compiler::Compiler;
use anyhow::*;
use clap::{AppSettings, Clap};
use std::fs::File;
use std::io::{self, Read};

/// FML compiler written by fellow human beings Vašek and Narek
#[derive(Clap)]
#[clap(version = "1.0", author = "Narek Vardanjan", author = "Vašek Král")]
#[clap(setting = AppSettings::ColoredHelp)]
struct Opts {
    // prints debug output instead
    #[clap(short, long)]
    debug: bool,
    // input file if missing stdout is used
    input: Option<String>,
}

fn main() -> Result<()> {
    let opts: Opts = Opts::parse();

    let r: Box<dyn Read> = match opts.input {
        None => Box::new(io::stdin()),
        Some(f) => {
            let file = File::open(f)?;
            Box::new(file)
        }
    };
    let ast_de: ast::AST = serde_json::from_reader(r)?;

    let program = Compiler::new().run(&ast_de)?;

    if opts.debug {
        println!("=== Const pool:");
        program.const_pool.iter().for_each(|i| println!("{:?}", i));
        println!();

        println!("=== Globals:");
        program.globals.iter().for_each(|i| println!("{:?}", i));
        println!();

        println!("Entry point (index of entry method): {}", program.entry);
    } else {
        program.to_bytes(&mut io::stdout())?;
    }

    Ok(())
}
