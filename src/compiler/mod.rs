pub mod frame;
pub mod instruction;
pub mod native;
pub mod program;
pub mod program_object;

use crate::ast::{Identifier, Operator, AST};
use crate::compiler::frame::Frame;
use crate::compiler::instruction::Instruction;
use crate::compiler::program::Program;
use crate::compiler::program_object::ProgramObject;
use anyhow::*;
use std::collections::HashMap;
use std::ops::Deref;

type Index = u16;

#[derive(Debug)]
pub struct Function {
    args: Vec<Identifier>,
    name: Index,
}

#[derive(Debug)]
pub struct Compiler {
    label_cnt: u16,
    const_pool: Vec<ProgramObject>,
    globals: Vec<Index>,
    entry: Index,
    global_frame: Frame,
    frame_stack: Vec<Frame>,
    functions: HashMap<String, Function>,
}

impl Compiler {
    pub fn new() -> Compiler {
        Compiler {
            label_cnt: 0,
            const_pool: Vec::new(),
            globals: Vec::new(),
            entry: 0,
            global_frame: Frame::new(),
            frame_stack: Vec::new(),
            functions: HashMap::new(),
        }
    }

    pub fn run(&mut self, ast: &AST) -> Result<Program> {
        let mut ins_buf: Vec<Instruction> = Vec::new();
        self.compile(ast, &mut ins_buf)?;
        Ok(Program {
            const_pool: self.const_pool.clone(),
            globals: self.globals.clone(),
            entry: self.entry,
        })
    }

    pub fn compile(&mut self, ast: &AST, ins_buf: &mut Vec<Instruction>) -> Result<()> {
        match ast {
            AST::Top(nodes) => {
                let name_ind = self.insert_str_into_const("λ:".to_owned());
                nodes.iter().try_for_each(|node| -> Result<()> {
                    self.compile(node, ins_buf)?;
                    self.pop_stack(ins_buf);
                    Ok(())
                })?;

                let method_ind = self.insert_into_const(ProgramObject::Method {
                    name: name_ind,
                    args: 0,
                    loc_n: self.global_frame.loc_n,
                    ins: ins_buf.clone(),
                });
                self.entry = method_ind;
                Ok(())
            }
            AST::Integer(i) => {
                let const_ind = self.insert_into_const(ProgramObject::Integer(*i));
                ins_buf.push(Instruction::Literal(const_ind));
                Ok(())
            }
            AST::Null => {
                let const_ind = self.insert_into_const(ProgramObject::Null);
                ins_buf.push(Instruction::Literal(const_ind));
                Ok(())
            }
            AST::Boolean(b) => {
                let const_ind = self.insert_into_const(ProgramObject::Bool(*b));
                ins_buf.push(Instruction::Literal(const_ind));
                Ok(())
            }
            AST::Print { format, arguments } => {
                let index = self.insert_str_into_const(format.clone());
                let args = arguments.len() as u8;
                arguments
                    .iter()
                    .try_for_each(|node| self.compile(node, ins_buf))?;
                ins_buf.push(Instruction::Print { index, args });
                Ok(())
            }
            AST::Function {
                name,
                body,
                parameters,
            } => {
                let (name_ind, instr) = self.compile_fun(name, body, parameters, Frame::new())?;

                ensure!(
                    self.functions
                        .insert(
                            name.0.clone(),
                            Function {
                                args: parameters.clone(),
                                name: name_ind,
                            },
                        )
                        .is_none(),
                    "function {} already defined",
                    name.0
                );

                let method_ind = self.insert_into_const(ProgramObject::Method {
                    name: name_ind,
                    args: parameters.len() as u8,
                    loc_n: self.frame_stack.last().as_ref().unwrap().loc_n,
                    ins: instr.clone(),
                });
                self.globals.push(method_ind);
                self.frame_stack.pop();
                let null_ind = self.insert_into_const(ProgramObject::Null);
                ins_buf.push(Instruction::Literal(null_ind));
                Ok(())
            }
            AST::CallFunction { name, arguments } => {
                arguments.iter().try_for_each(|a| -> Result<()> {
                    self.compile(a, ins_buf)?;
                    Ok(())
                })?;
                let fun = match self.functions.get(&name.0) {
                    Some(v) => v,
                    None => return Err(anyhow!("function {} is not defined", name)),
                };
                let args_n = fun.args.len() as u8;
                let name_ind = fun.name;
                ins_buf.push(Instruction::CallFunction {
                    args: args_n,
                    index: name_ind,
                });
                Ok(())
            }
            AST::Variable { name, value } => {
                self.compile(value, ins_buf)?;
                let ind = self.insert_str_into_const(name.0.clone());

                match &mut self.frame_stack.last_mut() {
                    //local variable in function
                    Some(f) => {
                        let loc_ind = f.loc_n;
                        f.alloc(name.clone(), loc_ind)?;
                        ins_buf.push(Instruction::SetLocal(loc_ind));
                        f.loc_n += 1;
                    }
                    //global variable
                    None if self.global_frame.scopes.len() == 1 => {
                        self.global_frame.alloc(name.clone(), ind)?;
                        ins_buf.push(Instruction::SetGlobal(ind));
                        let glob_ind = self.insert_into_const(ProgramObject::Slot(ind));
                        self.globals.push(glob_ind);
                    }
                    //local variable in top
                    None => {
                        let loc_ind = self.global_frame.loc_n;
                        self.global_frame.alloc(name.clone(), loc_ind)?;
                        ins_buf.push(Instruction::SetLocal(loc_ind));
                        self.global_frame.loc_n += 1;
                    }
                };
                Ok(())
            }
            AST::AccessVariable { name } => match self.seek_variable(name)? {
                (ind, true) => {
                    ins_buf.push(Instruction::GetGlobal(ind));
                    Ok(())
                }
                (ind, false) => {
                    ins_buf.push(Instruction::GetLocal(ind));
                    Ok(())
                }
            },
            AST::AssignVariable { name, value } => {
                self.compile(value, ins_buf)?;
                match self.seek_variable(name)? {
                    (ind, true) => {
                        ins_buf.push(Instruction::SetGlobal(ind));
                        Ok(())
                    }
                    (ind, false) => {
                        ins_buf.push(Instruction::SetLocal(ind));
                        Ok(())
                    }
                }
            }
            AST::Block(nodes) => {
                let frame = match self.frame_stack.last_mut() {
                    Some(f) => f,
                    None => &mut self.global_frame,
                };
                frame.scopes.push(HashMap::new());
                nodes.iter().try_for_each(|node| -> Result<()> {
                    self.compile(node, ins_buf)?;
                    //TODO: this should compare pointers, not values
                    if node != nodes.last().unwrap() {
                        self.pop_stack(ins_buf);
                    }
                    Ok(())
                })?;
                //ugly, but I can't use frame again (borrow shenanigans) :(
                match &mut self.frame_stack.last_mut() {
                    Some(f) => f.scopes.pop(),
                    None => self.global_frame.scopes.pop(),
                };
                Ok(())
            }
            AST::Object { extends, members } => {
                self.compile(extends, ins_buf)?;
                let v = members
                    .iter()
                    .map(|m| match m.as_ref() {
                        AST::Variable { name, value } => {
                            self.compile(value, ins_buf)?;
                            let name_ind = self.insert_str_into_const(name.0.clone());
                            let slot_ind = self.insert_into_const(ProgramObject::Slot(name_ind));
                            Ok(slot_ind)
                        }
                        AST::Function {
                            name,
                            body,
                            parameters,
                        } => {
                            let mut init_frame = Frame::new();
                            init_frame.alloc(Identifier::from("this"), 0)?;
                            init_frame.loc_n += 1;

                            let (name_ind, instr) =
                                self.compile_fun(name, body, parameters, init_frame)?;
                            let method_ind = self.insert_into_const(ProgramObject::Method {
                                name: name_ind,
                                args: (parameters.len() + 1) as u8,
                                loc_n: self.frame_stack.last().as_ref().unwrap().loc_n,
                                ins: instr.clone(),
                            });
                            self.frame_stack.pop();
                            Ok(method_ind)
                        }
                        _ => Err(anyhow!(
                            "object can only contain fields and methods, found {:?} instead",
                            m
                        )),
                    })
                    .collect::<Result<Vec<Index>>>()?;
                let obj_ind = self.insert_into_const(ProgramObject::Class(v));
                ins_buf.push(Instruction::Object(obj_ind));
                Ok(())
            }
            AST::CallMethod {
                object,
                name,
                arguments,
            } => {
                self.compile(object, ins_buf)?;
                arguments.iter().try_for_each(|a| -> Result<()> {
                    self.compile(a, ins_buf)?;
                    Ok(())
                })?;
                let args_n = (arguments.len() + 1) as u8;
                let name_ind = self.insert_str_into_const(name.0.clone());
                ins_buf.push(Instruction::CallMethod {
                    args: args_n,
                    index: name_ind,
                });
                Ok(())
            }
            AST::AccessField { object, field } => {
                self.compile(object, ins_buf)?;
                let ind = self.insert_str_into_const(field.0.clone());
                ins_buf.push(Instruction::GetField(ind));
                Ok(())
            }
            AST::AssignField {
                object,
                field,
                value,
            } => {
                self.compile(object, ins_buf)?;
                self.compile(value, ins_buf)?;
                let ind = self.insert_str_into_const(field.0.clone());
                ins_buf.push(Instruction::SetField(ind));
                Ok(())
            }
            AST::Conditional {
                condition,
                consequent,
                alternative,
            } => {
                self.compile(condition, ins_buf)?;
                let cons_l = self.next_label("if:cons:");
                let end_l = self.same_label("if:end:");
                let cons_ind = self.insert_str_into_const(cons_l);
                let end_ind = self.insert_str_into_const(end_l);
                ins_buf.push(Instruction::Branch(cons_ind));
                self.compile(alternative, ins_buf)?;
                ins_buf.push(Instruction::Jump(end_ind));
                ins_buf.push(Instruction::Label(cons_ind));
                self.compile(consequent, ins_buf)?;
                ins_buf.push(Instruction::Label(end_ind));
                Ok(())
            }
            AST::Loop { condition, body } => {
                let body_l = self.next_label("while:body:");
                let cond_l = self.same_label("while:cond:");
                let body_ind = self.insert_str_into_const(body_l);
                let cond_ind = self.insert_str_into_const(cond_l);
                let null_ind = self.insert_into_const(ProgramObject::Null);
                ins_buf.push(Instruction::Jump(cond_ind));
                ins_buf.push(Instruction::Label(body_ind));
                self.compile(body, ins_buf)?;
                self.pop_stack(ins_buf);
                ins_buf.push(Instruction::Label(cond_ind));
                self.compile(condition, ins_buf)?;
                ins_buf.push(Instruction::Branch(body_ind));
                ins_buf.push(Instruction::Literal(null_ind));
                Ok(())
            }
            AST::Array { size, value } => {
                match value.deref() {
                    AST::Integer(_)
                    | AST::Boolean(_)
                    | AST::Null
                    | AST::AccessVariable { .. }
                    | AST::AccessField { .. } => {
                        self.compile(size, ins_buf)?;
                        self.compile(value, ins_buf)?;
                        ins_buf.push(Instruction::Array);
                        Ok(())
                    }
                    _ => {
                        //local identifiers
                        let arr_id = Identifier(self.next_label("__array"));
                        let size_id = Identifier(self.same_label("__array_size"));
                        let i_id = Identifier(self.same_label("__array_ind"));

                        //saving the size of array
                        self.compile(&AST::variable(size_id.clone(), *size.clone()), ins_buf)?;
                        self.pop_stack(ins_buf);

                        //initializing array
                        self.compile(
                            &AST::variable(
                                arr_id.clone(),
                                AST::array(AST::access_variable(size_id.clone()), AST::null()),
                            ),
                            ins_buf,
                        )?;
                        //we are not dropping the array variable, so we can "return it" later on

                        //creating iterator
                        self.compile(&AST::variable(i_id.clone(), AST::integer(0)), ins_buf)?;
                        self.pop_stack(ins_buf);

                        //setting each index of the array
                        self.compile(
                            &AST::loop_de_loop(
                                //while condition
                                AST::operation(
                                    Operator::Less,
                                    AST::access_variable(i_id.clone()),
                                    AST::access_variable(size_id),
                                ),
                                //while body
                                AST::block(vec![
                                    //set current array index
                                    AST::assign_array(
                                        AST::access_variable(arr_id),
                                        AST::access_variable(i_id.clone()),
                                        *value.clone(),
                                    ),
                                    //increment iterator
                                    AST::assign_variable(
                                        i_id.clone(),
                                        AST::operation(
                                            Operator::Addition,
                                            AST::access_variable(i_id),
                                            AST::integer(1),
                                        ),
                                    ),
                                ]),
                            ),
                            ins_buf,
                        )?;
                        self.pop_stack(ins_buf);
                        Ok(())
                    }
                }
            }
            AST::AssignArray {
                array,
                index,
                value,
            } => {
                let name_ind = self.insert_str_into_const("set".to_owned());
                self.compile(array, ins_buf)?;
                self.compile(index, ins_buf)?;
                self.compile(value, ins_buf)?;
                ins_buf.push(Instruction::CallMethod {
                    args: 3, //self, index, value
                    index: name_ind,
                });
                Ok(())
            }
            AST::AccessArray { array, index } => {
                let name_ind = self.insert_str_into_const("get".to_owned());
                self.compile(array, ins_buf)?;
                self.compile(index, ins_buf)?;
                ins_buf.push(Instruction::CallMethod {
                    args: 2,
                    index: name_ind,
                });
                Ok(())
            }
        }
    }

    fn next_label(&mut self, prefix: &str) -> String {
        self.label_cnt += 1;
        self.same_label(prefix)
    }

    fn same_label(&mut self, prefix: &str) -> String {
        format!("{}{}", prefix, self.label_cnt - 1)
    }

    pub fn insert_into_const(&mut self, po: ProgramObject) -> Index {
        let index = self.const_pool.iter().position(|p| po == *p);
        match index {
            Some(p) => p as Index,
            None => {
                self.const_pool.push(po);
                self.const_pool.len() as Index - 1
            }
        }
    }

    pub fn insert_str_into_const(&mut self, str: String) -> Index {
        self.insert_into_const(ProgramObject::Str(str))
    }

    pub fn pop_stack(&mut self, ins_buf: &mut Vec<Instruction>) {
        ins_buf.push(Instruction::Drop);
    }

    //bool tells us, if the variable is global
    pub fn seek_variable(&mut self, name: &Identifier) -> Result<(Index, bool)> {
        match &self.frame_stack.last() {
            //search locals of function
            Some(f) => match f.seek(name) {
                Some(ind) => return Ok((ind, false)),
                None => (),
            },
            //search locals of top
            None => {
                for scope_ind in (1..self.global_frame.scopes.len()).rev() {
                    match self.global_frame.scopes[scope_ind].get(name) {
                        Some(ind) => return Ok((*ind, false)),
                        None => (),
                    }
                }
            }
        };

        //search globals
        match self.global_frame.scopes.first().unwrap().get(name) {
            Some(ind) => Ok((*ind, true)),
            None => Err(anyhow!("undefined variable '{}'", name.0)),
        }
    }

    pub fn compile_fun(
        &mut self,
        name: &Identifier,
        body: &AST,
        parameters: &Vec<Identifier>,
        init_frame: Frame,
    ) -> Result<(Index, Vec<Instruction>)> {
        let owned_name = name.0.clone();
        let name_ind = self.insert_str_into_const(owned_name);

        let frame = parameters.iter().try_fold(init_frame, |mut acc, id| {
            acc.loc_n += 1;
            acc.alloc(id.clone(), acc.loc_n - 1).map(|_| acc)
        })?;
        self.frame_stack.push(frame);

        let mut instr = Vec::new();
        self.compile(body, &mut instr)?;
        instr.push(Instruction::Return);
        Ok((name_ind, instr))
    }
}
