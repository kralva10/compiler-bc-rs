use anyhow::Result;
use std::io::Write;

pub trait LeWrite: Sized {
    fn le_write<W: Write>(num: Self, output: &mut W) -> Result<()>;
}

macro_rules! impl_le_write {
    ($type:ty) => {
        impl LeWrite for $type {
            fn le_write<W: Write>(num: Self, output: &mut W) -> Result<()> {
                output.write_all(&num.to_le_bytes())?;
                Ok(())
            }
        }
    };
}

impl_le_write!(u8);
impl_le_write!(u16);
impl_le_write!(u32);
impl_le_write!(i32);
