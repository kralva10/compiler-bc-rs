use crate::compiler::instruction::Instruction;
use crate::compiler::native::LeWrite;
use anyhow::*;
use std::io::Write;

type Index = u16;

#[derive(Clone, Debug, PartialEq)]
pub enum ProgramObject {
    Integer(i32),
    Bool(bool),
    Null,
    Str(String),
    Slot(Index),
    Method {
        name: Index,
        args: u8,
        loc_n: u16,
        ins: Vec<Instruction>,
    },
    Class(Vec<Index>),
}

impl ProgramObject {
    pub fn to_bytes<W: Write>(&self, output: &mut W) -> Result<()> {
        match self {
            ProgramObject::Integer(i) => {
                u8::le_write(0x00, output)?;
                i32::le_write(*i, output)
            }
            ProgramObject::Null => u8::le_write(0x01, output),
            ProgramObject::Str(s) => {
                u8::le_write(0x02, output)?;
                let bytes = s.as_bytes();
                u32::le_write(bytes.len() as u32, output)?;
                bytes.iter().try_for_each(|b| u8::le_write(*b, output))
            }
            ProgramObject::Method {
                name,
                args,
                loc_n,
                ins,
            } => {
                u8::le_write(0x03, output)?;
                u16::le_write(*name, output)?;
                u8::le_write(*args, output)?;
                u16::le_write(*loc_n, output)?;
                u32::le_write(ins.len() as u32, output)?;
                ins.iter().try_for_each(|i| i.to_bytes(output))
            }
            ProgramObject::Slot(index) => {
                u8::le_write(0x04, output)?;
                Index::le_write(*index, output)
            }
            ProgramObject::Class(v) => {
                u8::le_write(0x05, output)?;
                u16::le_write(v.len() as u16, output)?;
                v.iter().try_for_each(|i| u16::le_write(*i, output))
            }
            ProgramObject::Bool(b) => {
                u8::le_write(0x06, output)?;
                u8::le_write(if *b { 0x01 } else { 0x00 }, output)
            }
        }
    }
}
